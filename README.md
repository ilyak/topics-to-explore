# Embedded security project ideas

This is a list of refined ideas that I think can be suitable for student projects.

There is also a [raw list](https://gitlab.science.ru.nl/ilyak/crazyideas). It accessible to everyone with a Radboud Science account. 

## 1. Exploitation (and protection) of wireless IoT solutions

Many IoT devices are built around a wireless microcontroller.
I have not see a lot of studies telling if and how these solutions can be vulnerable, especially from the wireless side (think of [baseband attacks on mobile phones](https://www.usenix.org/system/files/conference/woot12/woot12-final24.pdf) as an inspiration).

The interesting part is that some of these wireless microcontrollers have not so standard architectures.
For instance, [ESP8266](https://en.wikipedia.org/wiki/ESP8266) and [ESP32](https://en.wikipedia.org/wiki/ESP32) chips, extremely popular in the maker community, are built around a [Tensilica Xtensa](https://en.wikipedia.org/wiki/Tensilica#Xtensa_configurable_cores) CPU core.
Some studies of this platform security were done [here](https://speakerdeck.com/jsandin/the-complete-esp8266-psionics-handbook).
Other examples of wireless microcontrollers are based on ARM Cortex-M, e.g. [TI CC2538](http://www.ti.com/product/CC2538).

The goal of the project would be to explore the landscape of the attacks, implement proof of concept attacks in practice, and develop/contribute to open source tooling for security evaluations and securing wireless IoT paltforms.

The hardware for this project should be relatively cheap.

Related work:
* [Xtensa & Mongoose OS exploitation](http://hardwear.io/carel-philipp.php)
* [The Complete ESP8266 Psionics Handbook](https://speakerdeck.com/jsandin/the-complete-esp8266-psionics-handbook)

### Launch a wireless IoT security Capture The Flag contest

A part of the project can be launching the small CTF with challenges build around typical IoT architecture. Xtensa should be particular fun.

## 2. Reasonable DPA/DEMA resistance with unprotected (symmetric) primitives

Side channel research community is focusing mostly on higly secure higher order masking schemes.
At the same time, I believe there is place for resonably secure cryptographic implementations that resists "shortcut" DPA (but not the advanced lab attacks) and run on microcontrollers available over-the-counter.
Because most IoT products are unlikely to feature tamper-resistant crypto hardware (because the latter noramlly comes unders NDA), at least in the close future.

These resonably protected implementations should be preferably generic, i.e. not crafted to leakage characteristics of particular HW.
So most likely not masking, but shuffling, dummy rounds/executions, re-keying etc.

A reasonable building block can be ARMv8-A crypto extensions. Though they are not available on Cortex-M which is more frequent in low-power devices. Wireless microcontrollers for such devices often have crypto accelerators.

An example of work in this direction is https://eprint.iacr.org/2017/663.pdf.
Implementation presented in https://cryptojedi.org/papers/aesarm-20160718.pdf can serve a reference on what can be achieved with simple masking (no practical effectiveness evaluation has been done there though).

The goal of the project would be to find and impement a solution, and also to produce a high-level overview paper discussing the approaches and opportunities for the designer.

## 3. Hardware password token security analysis

Try to break [Pastilda](https://www.crowdsupply.com/third-pin/pastilda) (also http://www.pastilda.com) in many ways.
It is based on STM32F415 - almost the same chip as in Piñata, so not very tamper-resistant.
Software exploitation (starting with fuzzing) form the host side is also in scope.
Pastilda's [design rationale](https://habrahabr.ru/post/305594/) has been written in Russian.

The exploration can be extended to other password managers, e.g. [Mooltipass](https://www.indiegogo.com/projects/mooltipass-open-source-offline-password-keeper#/). Other solutions were gathered by Pastilda developers in [this list](https://docs.google.com/spreadsheets/d/1q-vCx8QxtwEWVKSXha0V2XxxN7ILDdTP-DDhrjcpeiQ/edit#gid=0).


## 4. Extend open source DPA tooling

[Jlsca](https://github.com/Riscure/Jlsca) is an experimental high-performance DPA toolbox written in [Julia](https://julialang.org). Some [examples and tutorials](https://github.com/ikizhvatov/jlsca-tutorials) are available.

The goal of this project is to extend Jlsca with implementations of recent ciphers (Sponge-based, PRESENT etc.).

Another goal would be to benchmark (higher-order) attacks run with Jlsca on the cluster.

Both goals have a side effect of another pair of eyes looking at the Jlsca code and refining it.


## 5. Apply clustering methods in fault analysis

To be explored in collaboration with Riscure. Description provided by Albert Spruyt.

Fault injection is used to compromise the security of embedded systems.
Ways in which to induce faults in a target include: changing the supply voltage, changing the target’s clock or using laser or electromagnetic pulses.
 
When fault injection is performed on an open target, the attacker can first run profiling code.
This profiling code can give significant insight in the way that a target responds to the glitch.
The profiling code can send messages about the state of the target after a fault.
 
For example:
* If we receive output which is corrupted in the manner hoped for, we are successful.
* If we receive no output, we consider the target mute and that the glitch was probably too strong.
* If we receive the expected/uncorrupted output we consider that the glitch was perhaps not strong enough.
* If we receive corrupted output, the glitch was probably good, but the timing was off, but these parameters require further scrutiny.
 
In a black box scenario this information is often not available.
In fact it is in the defenders interest to give as little information as possible about the effects of the fault on the target.
This means makes it more difficult for an attacker to direct the parameter search.
 
However, using passive side channels it could be possible to recover some of the lost information.
For instance, when attacking secure boot on an embedded system the presence or absence of different digital signals (flash access) can give an attacker insight into the state of the target after a glitch.
There are additional ways to extract information from a target, for instance by measuring the power usage of the target or the electromagnetic emanations from it.
These measurements can easily grow by tens of gigabytes per day of testing.
The problem is how to extract meaningful information from this data, which allows an attacker to direct the parameter search in fault injection. 
 
A small proof of concept using clustering is available that can distinguish between mute traces and expected traces.
Various datasets are available and no actual fault injection needs to actually be performed if this is not desired.

## 6. Deep Learning for Side Channel Analysis

Reproduce approach from https://eprint.iacr.org/2017/740 on a very simple target, e.g. from [this tutorial](https://github.com/ikizhvatov/jlsca-tutorials/blob/master/rhme2-escalate.ipynb).

This will involve understanding and working with [Keras](https://github.com/fchollet/keras) deep learning library.
